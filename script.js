function CreateNewUser() {
    this.birthday = reverseBirthday(prompt("Please, enter your date of birth in format: dd.mm.yyyy", "dd.mm.yyyy"));
    
    this.firstName;
    while (!this.firstName || !isNaN(this.firstName)){
        this.firstName = prompt("Please, enter your first name!");
    }

    this.lastName;
    while (!this.lastName || !isNaN(this.lastName)){
        this.lastName = prompt("Please, enter your last name!");
    }

    this.getAge = function() {
        return ((new Date().getTime() - new Date(this.birthday)) / (24 * 3600 * 365.25 * 1000)) | 0;
    }

    this.getPassword = function(){
        return (this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(0, 4));
    };

    this.getLogin = function(){
        return (this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase());
    };

    Object.defineProperties(this, {
        firstName: {
            writable: false,
        },
        lastName: {
            writable: false,
        }
    });

    this.setFirstName = function(value) {
        Object.defineProperty(
            this,
            "firstName",
            {
                value: value
            }
        );
    };
    this.setLastName = function(value) {
        Object.defineProperty(
            this,
            "lastName",
            {
                value: value
            }
        );
    }
}

function reverseBirthday(birthday) {
    let reverseBirthdayArr = Array(birthday);
    return reverseBirthdayArr[0].toString().split('.').reverse().join('.');
}

let newUser = new CreateNewUser();

/* For test of setter */

/* newUser.setFirstName('Irbis'); */

console.log(newUser.getAge());
console.log(newUser.getPassword());




